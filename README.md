# asterixjs
Intended as a typescript/javascript-library for processing All-purpose structured EUROCONTROL surveillance information exchange (ASTERIX) message.

## Supported Categories
Not all Categories of the Asterix standard are supported by asterixjs.
Thereby a list of supported features will be provided:

### Cat62
Data-Items supported:
 - DataSourceidentifier
 - Service Identification
 - Time Of Track Information
 - Calculated Position in WGS-84 Co-ordinates
 - Calculated Position in Cartesian Co-ordinates
 - Calculated Track Velocity
 - Calculated Acceleration
 - Track Number
 - Measured Information


## Examples:
[https://tim-s.gitlab.io/asterixjs/example/](https://tim-s.gitlab.io/asterixjs/example/index.html) shows an example, where the _Calculated Position in WGS-84 Co-ordinates_ of a  (base64-encoded) Cat62 is marked on a map using [geojs](https://github.com/OpenGeoscience/geojs).

## Build
To build the asterixjs, type
```shell
yarn install
yarn build
```

Provided you have build asterixjs, you can build the example by typing
```shell
yarn build-example
``` 


## Run Tests
To get an up-to-date overview of the implemented DataItems,
you might have a look on test-reports by running the test with
```shell
yarn test
```

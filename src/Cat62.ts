import { bits, bitsSigned, flag } from "@tim-s/bi-ts";
import * as process from "process";

function FSPEC(octets: Uint8Array, offset: number = 0): IFSpec {
    let byteOffset = offset;
    let octet: number = octets[byteOffset];
    const isSet = (bit: number) => flag(bit, octet) ? true : false;
    const fields: boolean[] = [isSet(7), isSet(6), isSet(5), isSet(4), isSet(3), isSet(2), isSet(1)];
    while (isSet(0)) {
        octet = octets[++byteOffset];
        fields.push(isSet(7), isSet(6), isSet(5), isSet(4), isSet(3), isSet(2), isSet(1));
    }

    return {
        end: byteOffset,
        fields,
        start: offset,
    };
}

export interface ICat62 extends INode {
    dataSourceIdentifier: IDataSourceIdentifier;
    fspec: IFSpec;
    measuredInformation?: IMeasuredInformation;
    serviceIdentification?: IServiceIdentification;
    trackNumber: ITrackNumber;
    trackMode3ACode?: any; // TODO
    trackStatus: ITrackStatus;
    timeOfTrackInformation: ITimeOfTrackInformation;
    calculatedTrackGeometricAltitude?: IGeometricAltitude;
    calculatedTrackPositionInWGS84Coordinates?: IWGS84Coordinates;
    calculatedTrackPositionInCartesianCoordinates?: ICartesianCoordinates;
    calculatedTrackVelocityCartesian?: IVelocityCartesian;
    calculatedTrackAccelerationCartesian?: IAccelerationCartesian;
}

export interface INode {
    start: number;
    end: number;
}

export interface IFSpec extends INode {
    fields: boolean[];
}

export interface IDataSourceIdentifier extends INode {
    end: number;
    sac: number;
    sic: number;
}

export interface IServiceIdentification extends INode {
    id: number;
}

export interface ITimeOfTrackInformation extends INode {
    time: number;
}

export interface IWGS84Coordinates extends INode {
    latitude: number;
    longitude: number;
}

export interface ICartesianCoordinates extends INode {
    x: number;
    y: number;
}

export interface IVelocityCartesian extends INode {
    vx: number;
    vy: number;
}

export interface IAccelerationCartesian extends INode {
    Ax: number;
    Ay: number;
}

export interface ITrackStatus extends INode {
    fspec: boolean[];
}

export interface ITrackNumber extends INode {
    trackNumber: number;
}

export interface IGeometricAltitude extends INode {
    altitude: number;
}

export interface IMeasuredInformation extends INode {
    fspec: IFSpec;
    sensorIdentification?: IDataSourceIdentifier;
    lastMeasuredModeCCode?: ILastMeasuredModeCCode;
    lastMeasuredMode3ACode?: ILastMeasured3ACode;
    measuredPosition?: IMeasuredPosition;
    reportType?: IReportType;
}

export interface IMeasuredPosition extends INode {
    azimuth: number;
    distance: number;
}

export interface IReportType extends INode {
    type: EReportType;
    fixedTransponder: boolean;
    simulated: boolean;
    testTarget: boolean;
}

export enum EReportType {
    NoDetection = 0,
    SinglePsrDetection = 1,
    SingleSsrDetection = 2,
    SsrPsrDetection = 3,
    SingleModeSAllCall = 4,
    SingleModeSRollCall = 5,
    ModeSAllCallPlusPsr = 6,
    ModeSRollCallPlusPsr = 7,
}

export interface ILastMeasured3ACode extends INode {
    garbled: boolean;
    notValidated: boolean;
    smoothedMode: boolean;
    transponderCode: string; // <- octal string
}

export interface ILastMeasuredModeCCode extends INode {
    garbled: boolean;
    notValidated: boolean;
    height: number;
}

export function Cat62(octets: Uint8Array, offset: number = 0): ICat62 {
    let nextPos = offset;
    const fspec = FSPEC(octets, nextPos);
    nextPos = fspec.end + 1;

    if (! fspec.fields[0]) {
        throw new Error("Missing mandatory Data Source Identifier in FSPEC!");
    }
    const dataSourceIdentifier = DataSourceIdentifier(octets, nextPos);
    nextPos = dataSourceIdentifier.end + 1;

    let serviceIdentification;
    if (fspec.fields[2]) {
        serviceIdentification = ServiceIdentification(octets, nextPos);
        nextPos = serviceIdentification.end + 1;
    }

    if (! fspec.fields[3]) {
        throw new Error("Missing mandatory Time Of Track Information in FSPEC!");
    }
    const timeOfTrackInformation = TimeOfTrackInformation(octets, nextPos);
    nextPos = timeOfTrackInformation.end + 1;

    let calculatedTrackPositionInWGS84Coordinates;
    if (fspec.fields[4]) {
        calculatedTrackPositionInWGS84Coordinates = CalculatedTrackPositionInWGS84Coordinates(octets, nextPos);
        nextPos = calculatedTrackPositionInWGS84Coordinates.end + 1;
    }

    let calculatedTrackPositionInCartesianCoordinates;
    if (fspec.fields[5]) {
        calculatedTrackPositionInCartesianCoordinates =
            CalculatedTrackPositionInCartesianCoordinates(octets, nextPos);
        nextPos = calculatedTrackPositionInCartesianCoordinates.end + 1;
    }

    let calculatedTrackVelocityCartesian;
    if (fspec.fields[6]) {
        calculatedTrackVelocityCartesian = CalculatedTrackVelocityCartesian(octets, nextPos);
        nextPos = calculatedTrackVelocityCartesian.end + 1;
    }

    let calculatedTrackAccelerationCartesian;
    if (fspec.fields[7]) {
        calculatedTrackAccelerationCartesian = CalculatedAccelerationCartesian(octets, nextPos);
        nextPos = calculatedTrackAccelerationCartesian.end + 1;
    }

    let tm3ac;
    if (fspec.fields[8]) {
        tm3ac = TrackMode3ACode(octets, nextPos);
        nextPos = tm3ac.end + 1;
    }

    let add;
    if (fspec.fields[10]) {
        add = AircraftDerivedData(octets, nextPos);
        nextPos = add.end + 1;
    }

    if (! fspec.fields[11]) {
        throw new Error("Missing mandatory Track Number in FSPEC!");
    }
    const trackNumber = TrackNumber(octets, nextPos);
    nextPos = trackNumber.end + 1;

    if (! fspec.fields[12]) {
        throw new Error("Missing mandatory Track Status in FSPEC!");
    }
    const trackStatus = TrackStatus(octets, nextPos);
    nextPos = trackStatus.end + 1;

    let systemTrackUpdateAges;
    if (fspec.fields[13]) {
        systemTrackUpdateAges = SystemTrackUpdateAges(octets, nextPos);
        nextPos = systemTrackUpdateAges.end + 1;
    }

    let modeOfMovement;
    if (fspec.fields[14]) {
        modeOfMovement = ModeOfMovement(octets, nextPos);
        nextPos = modeOfMovement.end + 1;
    }

    let trackDataAges;
    if (fspec.fields[15]) {
        trackDataAges = TrackDataAges(octets, nextPos);
        nextPos = trackDataAges.end + 1;
    }

    let measuredFlightLevel;
    if (fspec.fields[16]) {
        measuredFlightLevel = MeasuredFlightLevel(octets, nextPos);
        nextPos = measuredFlightLevel.end + 1;
    }

    let calculatedTrackGeometricAltitude;
    if (fspec.fields[17]) {
        calculatedTrackGeometricAltitude = CalculatedTrackGeometricAltitude(octets, nextPos);
        nextPos = calculatedTrackGeometricAltitude.end + 1;
    }

    let calculatedTrackBarometricAltitude;
    if (fspec.fields[18]) {
        calculatedTrackBarometricAltitude = CalculatedTrackBarometricAltitude(octets, nextPos);
        nextPos = calculatedTrackBarometricAltitude.end + 1;
    }

    let calculatedRateOfClimbDescend;
    if (fspec.fields[19]) {
        calculatedRateOfClimbDescend = CalculatedrateOfClimbDescend(octets, nextPos);
        nextPos = calculatedRateOfClimbDescend.end + 1;
    }

    let measuredInformation;
    if (fspec.fields[27]) {
        measuredInformation = MeasuredInformation(octets, nextPos);
        nextPos = measuredInformation.end + 1;
    }

    return {
        // add,
        calculatedTrackAccelerationCartesian,
        // calculatedRateOfClimbDescend,
        // calculatedTrackBarometricAltitude,
        calculatedTrackGeometricAltitude,
        calculatedTrackPositionInCartesianCoordinates,
        calculatedTrackPositionInWGS84Coordinates,
        calculatedTrackVelocityCartesian,
        dataSourceIdentifier,
        end: nextPos - 1,
        fspec,
        // measuredFlightLevel,
        measuredInformation,
        // modeOfMovement,
        serviceIdentification,
        start: offset,
        // systemTrackUpdateAges,
        timeOfTrackInformation,
        // trackDataAges,
        trackNumber,
        trackStatus,
    };
}

function DataSourceIdentifier(octets: Uint8Array, offset: number = 0): IDataSourceIdentifier {
    return {
        end: offset + 1,
        sac: octets[offset],
        sic: octets[offset + 1],
        start: offset,
    };
}

function ServiceIdentification(octets: Uint8Array, offset: number = 0): IServiceIdentification {
    return {
        end: offset,
        id: octets[offset],
        start: offset,
    };
}

function TimeOfTrackInformation(octets: Uint8Array, offset: number = 0): ITimeOfTrackInformation {
    let byteOffset = offset;
    const toti = ((octets[byteOffset] * 256 * 256) + (octets[++byteOffset] * 256) + octets[++byteOffset]);

    // time of day in seconds:
    const tod = toti / 128;

    return {
        end: offset + 2,
        start: offset,
        time: tod,
    };
}

function CalculatedTrackPositionInWGS84Coordinates(octets: Uint8Array, offset: number = 0): IWGS84Coordinates {
    const dv = new DataView(octets.buffer, offset);
    const lat = dv.getInt32(0);
    const lon = dv.getInt32(4);
    return {
        end: offset + 7,
        latitude: lat * 180 / Math.pow(2, 25),
        longitude: lon * 180 / Math.pow(2, 25),
        start: offset,
    };
}

function CalculatedTrackPositionInCartesianCoordinates(octets: Uint8Array, offset: number = 0): ICartesianCoordinates {
    // tslint:disable:no-bitwise
    const isXNegative = (octets[offset] >> 7 ) ? true : false;
    const isYNegative = octets[offset + 3] & ( 1 << 7 ) ? true : false;
    // tslint:enable:no-bitwise

    // pad 3byte blocks for x/y to 4byte blocks each:
    const padded = Uint8Array.of(
        isXNegative ? 0xFF : 0x00, octets[offset], octets[offset + 1], octets[offset + 2],
        isYNegative ? 0xFF : 0x00, octets[offset + 3], octets[offset + 4], octets[offset + 5],
    );

    const dv = new DataView(padded.buffer);
    const x = dv.getInt32(0);
    const y = dv.getInt32(4);

    return {
        end: offset + 5,
        start: offset,
        x : x / 2,
        y : y / 2,
    };
}

function CalculatedTrackVelocityCartesian(octets: Uint8Array, offset: number = 0): IVelocityCartesian {
    const dv = new DataView(octets.buffer, offset);
    const vx = dv.getInt16(0);
    const vy = dv.getInt16(2);

    return {
        end: offset + 3,
        start: offset,
        vx : vx / 4,
        vy : vy / 4,
    };
}

function CalculatedAccelerationCartesian(octets: Uint8Array, offset: number = 0): IAccelerationCartesian {
    const dv = new DataView(octets.buffer, offset);
    const Ax = dv.getInt8(0);
    const Ay = dv.getInt8(1);

    return {
        Ax : Ax / 4,
        Ay : Ay / 4,
        end: offset + 1,
        start: offset,
    };
}

function TrackMode3ACode(octets: Uint8Array, offset: number = 0) {
    const dv = new DataView(octets.buffer, offset);
    // TODO
    return {
        end: offset + 1,
    };
}

function AircraftDerivedData(octets: Uint8Array, offset: number = 0) {
    const fspec = FSPEC(octets, offset);
    let currentOffset = fspec.end + 1;

    let ta;
    if (fspec.fields[0]) {
        ta = TargetAddress(octets, currentOffset);
        currentOffset = ta.end + 1;
    }

    let ti;
    if (fspec.fields[1]) {
        ti = TargetIdentification(octets, currentOffset);
        currentOffset = ti.end + 1;
    }

    // TODO Cases for fspec [2..8]

    let i062380;
    if (fspec.fields[9]) {
        i062380 = I062380(octets, currentOffset);
        currentOffset = i062380.end + 1;
    }

    return {
        end: currentOffset - 1,
        ta,
        ti,
    };
}

function TargetAddress(octets: Uint8Array, offset: number = 0) {
    const dv = new DataView(octets.buffer, offset);
    // TODO
    return {
        end: offset + 2,
    };
}

function TargetIdentification(octets: Uint8Array, offset: number = 0) {
    const dv = new DataView(octets.buffer, offset);
    // TODO
    return {
        end: offset + 5,
    };
}

function I062380(octets: Uint8Array, offset: number = 0) {
    const dv = new DataView(octets.buffer, offset);
    // TODO
    return {
        end: offset + 1,
    };
}

function TrackNumber(octets: Uint8Array, offset: number = 0): ITrackNumber {
    const dv = new DataView(octets.buffer, offset);
    return {
        end: offset + 1,
        start: offset,
        trackNumber: dv.getUint16(0),
    };
}

function TrackStatus(octets: Uint8Array, offset: number = 0): ITrackStatus {
    const fspec = FSPEC(octets, offset);
    return {
        end: fspec.end,
        fspec: fspec.fields,
        start: offset,
    };
}

function SystemTrackUpdateAges(octets: Uint8Array, offset: number = 0) {
    const fspec = FSPEC(octets, offset);
    let currentOffset = fspec.end + 1;

    let ta;
    if (fspec.fields[0]) {
        ta = TrackAge(octets, currentOffset);
        currentOffset = ta.end + 1;
    }
    let pa;
    if (fspec.fields[1]) {
        pa = PSRAge(octets, currentOffset);
        currentOffset = pa.end + 1;
    }
    let sa;
    if (fspec.fields[2]) {
        sa = SSRAge(octets, currentOffset);
        currentOffset = sa.end + 1;
    }
    let msa;
    if (fspec.fields[3]) {
        msa = ModeSAge(octets, currentOffset);
        currentOffset = msa.end + 1;
    }
    let aa;
    if (fspec.fields[4]) {
        aa = ADSCAge(octets, currentOffset);
        currentOffset = aa.end + 1;
    }
    let ea;
    if (fspec.fields[5]) {
        ea = ESAge(octets, currentOffset);
        currentOffset = ea.end + 1;
    }
    let va;
    if (fspec.fields[6]) {
        va = VDLAge(octets, currentOffset);
        currentOffset = va.end + 1;
    }
    let ua;
    if (fspec.fields[7]) {
        ua = UATAge(octets, currentOffset);
        currentOffset = ua.end + 1;
    }
    let la;
    if (fspec.fields[8]) {
        la = LoopAge(octets, currentOffset);
        currentOffset = la.end + 1;
    }
    let ma;
    if (fspec.fields[9]) {
        ma = MultilaterationAge(octets, currentOffset);
        currentOffset = ma.end + 1;
    }

    return {
        aa,
        ea,
        end: currentOffset - 1,
        la,
        ma,
        msa,
        pa,
        sa,
        ta,
        ua,
        va,
    };
}

function TrackAge(octets: Uint8Array, offset: number = 0) {
    return {
        end: offset,
        trackAge: octets[offset] / 4,
    };
}

function PSRAge(octets: Uint8Array, offset: number = 0) {
    return {
        end: offset,
        psrAge: octets[offset] / 4,
    };
}

function SSRAge(octets: Uint8Array, offset: number = 0) {
    return {
        end: offset,
        ssrAge: octets[offset] / 4,
    };
}

function ModeSAge(octets: Uint8Array, offset: number = 0) {
    return {
        end: offset,
        modeSAge: octets[offset] / 4,
    };
}

function ADSCAge(octets: Uint8Array, offset: number = 0) {
    return {
        aDSCAge: octets[offset] / 4,
        end: offset,
    };
}

function ESAge(octets: Uint8Array, offset: number = 0) {
    return {
        eSAge: octets[offset] / 4,
        end: offset,
    };
}

function VDLAge(octets: Uint8Array, offset: number = 0) {
    return {
        end: offset,
        vDLAge: octets[offset] / 4,
    };
}

function UATAge(octets: Uint8Array, offset: number = 0) {
    return {
        end: offset,
        uATAge: octets[offset] / 4,
    };
}

function LoopAge(octets: Uint8Array, offset: number = 0) {
    return {
        end: offset,
        loopAge: octets[offset] / 4,
    };
}

function MultilaterationAge(octets: Uint8Array, offset: number = 0) {
    return {
        end: offset,
        multilaterationAge: octets[offset] / 4,
    };
}

function ModeOfMovement(octets: Uint8Array, offset: number = 0) {
    // TODO
    return {
        end: offset,
    };
}

function TrackDataAges(octets: Uint8Array, offset: number = 0) {
    const fspec = FSPEC(octets, offset);
    let currentOffset = fspec.end + 1;

    let mfla;
    if (fspec.fields[0]) {
        mfla = MeasuredFlightLevelAge(octets, currentOffset);
        currentOffset = mfla.end + 1;
    }
    let m1a;
    if (fspec.fields[1]) {
        m1a = Mode1Age(octets, currentOffset);
        currentOffset = m1a.end + 1;
    }

    let m2a;
    if (fspec.fields[2]) {
        m2a = Mode2Age(octets, currentOffset);
        currentOffset = m2a.end + 1;
    }

    let m3aa;
    if (fspec.fields[3]) {
        m3aa = Mode3AAge(octets, currentOffset);
        currentOffset = m3aa.end + 1;
    }

    // TODO implement remaining age fields...

    return {
        end: currentOffset - 1,
        m1a,
        m2a,
        m3aa,
        mfla,
    };
}

function MeasuredFlightLevelAge(octets: Uint8Array, offset: number = 0) {
    return {
        end: offset,
        measuredFlightLevelAge: octets[offset] / 4,
    };
}

function Mode1Age(octets: Uint8Array, offset: number = 0) {
    return {
        end: offset,
        mode1Age: octets[offset] / 4,
    };
}

function Mode2Age(octets: Uint8Array, offset: number = 0) {
    return {
        end: offset,
        mode2Age: octets[offset] / 4,
    };
}

function Mode3AAge(octets: Uint8Array, offset: number = 0) {
    return {
        end: offset,
        mode3AAge: octets[offset] / 4,
    };
}

function MeasuredFlightLevel(octets: Uint8Array, offset: number = 0) {
    const dv = new DataView(octets.buffer, offset);
    return {
        end: offset + 1,
        measuredFlightLevel: dv.getInt16(0) / 4,
    };
}

function CalculatedTrackGeometricAltitude(octets: Uint8Array, offset: number = 0): IGeometricAltitude {
    const dv = new DataView(octets.buffer, offset);
    return {
        altitude: dv.getInt16(0) * 6.25,
        end: offset + 1,
        start: offset,
    };
}

function CalculatedTrackBarometricAltitude(octets: Uint8Array, offset: number = 0) {
    const field = new DataView(octets.buffer, offset).getUint16(0);
    const qNHCorrectionApplied = flag(15, field) ? true : false;
    const calculatedTrackBarometricAltitude =  bitsSigned(14, 0, field) * 25;
    return {
        calculatedTrackBarometricAltitude,
        end: offset + 1,
        qNHCorrectionApplied,
    };
}

function CalculatedrateOfClimbDescend(octets: Uint8Array, offset: number = 0) {
    const dv = new DataView(octets.buffer, offset);
    const calculatedrateOfClimbDescend = dv.getInt16(0) * 6.25;
    return {
        calculatedrateOfClimbDescend,
        end: offset + 1,
    };
}

function MeasuredInformation(octets: Uint8Array, offset: number = 0): IMeasuredInformation {
    const fspec = FSPEC(octets, offset);
    let currentOffset = fspec.end + 1;

    let sensorIdentification;
    if (fspec.fields[0]) {
        sensorIdentification = DataSourceIdentifier(octets, currentOffset);
        currentOffset = sensorIdentification.end + 1;
    }

    let measuredPosition;
    if (fspec.fields[1]) {
        measuredPosition = MeasuredPosition(octets, currentOffset);
        currentOffset = measuredPosition.end + 1;
    }

    // TODO implement remaining subfields...

    let lastMeasuredModeCCode;
    if (fspec.fields[3]) {
        lastMeasuredModeCCode = LastMeasuredModeCCode(octets, currentOffset);
        currentOffset = lastMeasuredModeCCode.end + 1;
    }

    let lastMeasuredMode3ACode;
    if (fspec.fields[4]) {
        lastMeasuredMode3ACode = LastMeasuredMode3ACode(octets, currentOffset);
        currentOffset = lastMeasuredMode3ACode.end + 1;
    }

    let reportType;
    if (fspec.fields[5]) {
        reportType = ReportType(octets, currentOffset);
        currentOffset = reportType.end + 1;
    }
    // TODO implement remaining subfields...

    return {
        end: currentOffset - 1,
        fspec,
        lastMeasuredMode3ACode,
        lastMeasuredModeCCode,
        measuredPosition,
        reportType,
        sensorIdentification,
        start: offset,
    };
}

function MeasuredPosition(octets: Uint8Array, offset: number = 0): IMeasuredPosition {
    const dv = new DataView(octets.buffer, offset);
    return {
        azimuth: (dv.getUint16(2) * 360) / Math.pow(2, 16),
        distance: dv.getUint16(0) / 256,
        end: offset + 3,
        start: offset,
    };
}

function LastMeasuredModeCCode(octets: Uint8Array, offset: number = 0): ILastMeasuredModeCCode {
    const field = new DataView(octets.buffer, offset).getUint16(0);
    return {
        end: offset + 1,
        garbled: flag(14, field) ? true : false,
        height: bits(13, 0, field) / 4,
        notValidated: flag(15, field) ? true : false,
        start: offset,
    };
}

function LastMeasuredMode3ACode(octets: Uint8Array, offset: number = 0): ILastMeasured3ACode {
    const field = new DataView(octets.buffer, offset).getUint16(0);
    return {
        end: offset + 1,
        garbled: flag(14, field) ? true : false,
        notValidated: flag(15, field) ? true : false,
        smoothedMode: flag(13, field) ? true : false,
        start: offset,
        transponderCode: `0${bits(11, 0, field).toString(8)}`,
    };
}

function ReportType(octets: Uint8Array, offset: number = 0): IReportType {
    return {
        end: offset,
        fixedTransponder: flag(3, octets[offset]) ? true : false,
        simulated: flag(4, octets[offset]) ? true : false,
        start: offset,
        testTarget: flag(2, octets[offset]) ? true : false,
        type: bits(7, 5, octets[offset]),
    };
}

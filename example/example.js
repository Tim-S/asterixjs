require('d3/d3.min.js');
require('hammerjs/hammer.min.js');
var geo = require('geojs/geo.min.js');
var Cat62 = require('../dist/Cat62.js').Cat62;

$(function () {
    var map = geo.map({
        node: "#map"
    });
    map.createLayer('osm');
    var layer = map.createLayer('feature');
    var base64encodedCat62 = 'v9/9AhlkAVmBswB1GPwALK7Z/xlr8IZgA5P/QwAAAr3BIEynqEmUsd9A4CD2EmkZAwEIcBcNDQCQDQ0GGBbNBhgAANwZDJO6iOgGGAK9oA==';

    var cat62 = Cat62(toUint8Array(base64encodedCat62), 0);
    var coords = cat62.calculatedTrackPositionInWGS84Coordinates;
    var position = { x: coords.longitude, y: coords.latitude };
    map.center(position).zoom(14);

    var feature = layer.createFeature('point').data([position]).draw();

    var ui = map.createLayer('ui', { zIndex: 2 });
    var tooltip = ui.createWidget('dom', { position: position });
    var tooltipElem = $(tooltip.canvas()).attr('id', 'tooltip').html(`<pre><code>${JSON.stringify(cat62, null, 4)}</pre></code>`);
})

function toUint8Array(base64){
    var raw = window.atob(base64);
    var rawLength = raw.length;
    var array = new Uint8Array(new ArrayBuffer(rawLength));

    for(i = 0; i < rawLength; i++) {
        array[i] = raw.charCodeAt(i);
    }
    return array;
}
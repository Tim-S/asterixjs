import { expect} from "chai";
import { readFileSync } from "fs";
import "mocha";
import { join } from "path";
import { Cat62, EReportType, ICat62 } from "../src/Cat62";

describe("Cat62()", () => {
    const testDataPath = join(__dirname, "../data/cat62.raw");
    let cat62data: Uint8Array;
    let cat62: ICat62;
    let parseTime: number;

    before((done) => {
        cat62data = new Uint8Array(readFileSync(testDataPath));
        const hrstart = process.hrtime();
        cat62 = Cat62(cat62data, 0);
        const hrend = process.hrtime(hrstart);
        parseTime = hrend[0] * 1000 + hrend[1] / 1000000;
        done();
    });

    after((done) => {
        // tslint:disable-next-line:no-console
        console.log(`Parsing ${testDataPath} took ${parseTime}ms`);
        done();
    });

    it("should parse test-data in data/cat62.raw completely (i.e. byte #0 to #78)", () => {
        expect(cat62.end).to.equal(78);
    });

    it("should parse the DataSourceidentifier (SAC: 25, SIC: 100)", () => {
        expect(cat62.dataSourceIdentifier.sac).to.equal(25);
        expect(cat62.dataSourceIdentifier.sic).to.equal(100);
    });

    it("should parse the Service Identification (SI: 1)", () => {
        expect(cat62.serviceIdentification).to.deep.include({ id: 1 });
    });

    it("should parse the Time Of Track Information (45827.398 s)", () => {
        expect(cat62.timeOfTrackInformation.time.toFixed(3)).to.equal(45827.398.toFixed(3));
    });

    it("should parse the Calculated Position in WGS-84 Co-ordinates " +
        "(lat[deg]: 41.1671233177185, lon[deg]: 15.7088667154312)", () => {
        // tslint:disable-next-line:no-unused-expression
        expect(cat62.calculatedTrackPositionInWGS84Coordinates, "calculatedTrackPositionInWGS84Coordinates")
                .to.not.be.undefined;

        if (cat62.calculatedTrackPositionInWGS84Coordinates) {
            const coords = cat62.calculatedTrackPositionInWGS84Coordinates;
            expect(coords.latitude.toFixed(13)).to.equal(41.1671233177185.toFixed(13));
            expect(coords.longitude.toFixed(13)).to.equal(15.7088667154312.toFixed(13));
        }
    });

    it("should parse the Calculated Position in Cartesian Co-ordinates " +
        "(X[m]: -29514.5, Y[m]: -507088)", () => {

        expect(cat62).to.deep.include({
            calculatedTrackPositionInCartesianCoordinates: {
                end: 23,
                start: 18,
                x: -29514.5,
                y: -507088,
            },
        });
    });

    it("should parse the Calculated Track Velocity (vx[m/s]: 228.75, vy[m/s]: -47.25)", () => {
        expect(cat62).to.deep.include({
            calculatedTrackVelocityCartesian: {
                end: 27,
                start: 24,
                vx: 228.75,
                vy: -47.25,
            },
        });
    });

    it("should parse the Calculated Acceleration (Ax[m/s^2]: 0, Ay[m/s^2]: 0)", () => {
        expect(cat62).to.deep.include({
            calculatedTrackAccelerationCartesian: {
                Ax: 0,
                Ay: 0,
                end: 29,
                start: 28,
            },
        });
    });

    it("should parse the Track Number (4713)", () => {
        expect(cat62.trackNumber.trackNumber).to.equal(4713);
    });

    it("should parse the Measured Information", () => {
        expect(cat62).to.deep.include({
            measuredInformation: {
                end: 78,
                fspec: {
                    end: 67,
                    fields: [
                        true,
                        true,
                        false,
                        true,
                        true,
                        true,
                        false,
                    ],
                    start: 67,
                },
                lastMeasuredMode3ACode: {
                    end: 77,
                    garbled: false,
                    notValidated: false,
                    smoothedMode: false,
                    start: 76,
                    transponderCode: "01275",
                },
                lastMeasuredModeCCode: {
                    end: 75,
                    garbled: false,
                    height: 390,
                    notValidated: false,
                    start: 74,
                },
                measuredPosition: {
                    azimuth: 192.5244140625,
                    distance: 147.7265625,
                    end: 73,
                    start: 70,
                },
                reportType: {
                    end: 78,
                    fixedTransponder: false,
                    simulated: false,
                    start: 78,
                    testTarget: false,
                    type: EReportType.SingleModeSRollCall,
                },
                sensorIdentification: {
                    end: 69,
                    sac: 25,
                    sic: 12,
                    start: 68,
                },
                start: 67,
            },
        });
    });
});
